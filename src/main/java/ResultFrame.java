import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ResultFrame extends JFrame {
    private JPanel contentPane;
    private JTextArea resLabel;

    public ResultFrame(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 706, 546);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        resLabel = new JTextArea();
        resLabel.setBounds(10, 11, 670, 485);
        contentPane.add(resLabel);
    }

    public void setResLabel(StringBuilder s){
        this.resLabel.setText(s.toString());
    }
}
