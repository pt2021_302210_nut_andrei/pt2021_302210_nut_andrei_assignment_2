import java.util.List;

public class ConcreteStrategyQueue implements Strategy{
    @Override
    public void addTask(List<Server> servers, Task t) {
        int minLength = 0;
        for(Server x : servers){
            if(x.getTasks().isEmpty()){
                Thread th = new Thread(servers.get(servers.indexOf(x)));
                servers.get(servers.indexOf(x)).addTask(t);
                th.start();
                return;
            }
            if(x.getTasks().size() < servers.get(minLength).getTasks().size()){
                minLength = servers.indexOf(x);
            }
        }
        servers.get(minLength).addTask(t);
    }
}
