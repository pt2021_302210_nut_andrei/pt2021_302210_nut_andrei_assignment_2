import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class SimulationFrame extends JFrame {

    private JPanel contentPane;
    private JTextField timeLimit;
    private JTextField maxProcTime;
    private JTextField minProcTime;
    private JTextField nbOfSvs;
    private JTextField nbOfClients;
    private JTextField maxTasksPerSv;
    private JLabel lblStrategy;
    private JComboBox<SelectionPolicy> comboBox;
    private JLabel lblMaxTasks;
    private JLabel lblNbOfClients;
    private JLabel lblNbOfServers;
    private JLabel minProcTimeLbl;
    private JLabel maxProcTimeLbl;
    private JButton startBtn;
    private JLabel timeLimitLabel;
    private JLabel lblMinAr;
    private JTextField minArrTime;
    private JLabel lblMaxArrivalTime;
    private JTextField maxArrTime;

    public SimulationFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 470, 435);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        timeLimitLabel = new JLabel("Time limit:");
        timeLimitLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
        timeLimitLabel.setBounds(23, 16, 73, 38);
        contentPane.add(timeLimitLabel);

        startBtn = new JButton("START");
        startBtn.setBounds(159, 365, 147, 20);
        contentPane.add(startBtn);

        timeLimit = new JTextField();
        timeLimit.setBounds(106, 27, 86, 20);
        contentPane.add(timeLimit);
        timeLimit.setColumns(10);

        maxProcTimeLbl = new JLabel("Max Processing Time:");
        maxProcTimeLbl.setFont(new Font("Tahoma", Font.PLAIN, 15));
        maxProcTimeLbl.setBounds(23, 133, 147, 31);
        contentPane.add(maxProcTimeLbl);

        maxProcTime = new JTextField();
        maxProcTime.setColumns(10);
        maxProcTime.setBounds(192, 144, 86, 20);
        contentPane.add(maxProcTime);

        minProcTimeLbl = new JLabel("Min Processing Time:");
        minProcTimeLbl.setFont(new Font("Tahoma", Font.PLAIN, 15));
        minProcTimeLbl.setBounds(23, 175, 147, 30);
        contentPane.add(minProcTimeLbl);

        minProcTime = new JTextField();
        minProcTime.setColumns(10);
        minProcTime.setBounds(180, 186, 86, 19);
        contentPane.add(minProcTime);

        lblNbOfServers = new JLabel("Nb of Servers:");
        lblNbOfServers.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblNbOfServers.setBounds(23, 54, 102, 38);
        contentPane.add(lblNbOfServers);

        nbOfSvs = new JTextField();
        nbOfSvs.setColumns(10);
        nbOfSvs.setBounds(132, 65, 86, 20);
        contentPane.add(nbOfSvs);

        lblNbOfClients = new JLabel("Nb of Clients:");
        lblNbOfClients.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblNbOfClients.setBounds(23, 91, 102, 38);
        contentPane.add(lblNbOfClients);

        nbOfClients = new JTextField();
        nbOfClients.setColumns(10);
        nbOfClients.setBounds(135, 102, 86, 20);
        contentPane.add(nbOfClients);

        lblMaxTasks = new JLabel("Max Tasks / Server:");
        lblMaxTasks.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblMaxTasks.setBounds(23, 284, 147, 31);
        contentPane.add(lblMaxTasks);

        maxTasksPerSv = new JTextField();
        maxTasksPerSv.setColumns(10);
        maxTasksPerSv.setBounds(192, 295, 86, 20);
        contentPane.add(maxTasksPerSv);

        SelectionPolicy[] select = new SelectionPolicy[]{ SelectionPolicy.SHORTEST_TIME, SelectionPolicy.SHORTEST_QUEUE};
        comboBox = new JComboBox<SelectionPolicy>(select);
        comboBox.setBounds(106, 326, 186, 23);
        contentPane.add(comboBox);

        lblStrategy = new JLabel("Strategy:");
        lblStrategy.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblStrategy.setBounds(23, 316, 73, 38);
        contentPane.add(lblStrategy);

        lblMinAr = new JLabel("Min Arrival Time:");
        lblMinAr.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblMinAr.setBounds(23, 216, 147, 30);
        contentPane.add(lblMinAr);

        minArrTime = new JTextField();
        minArrTime.setColumns(10);
        minArrTime.setBounds(180, 227, 86, 19);
        contentPane.add(minArrTime);

        lblMaxArrivalTime = new JLabel("Max Arrival Time:");
        lblMaxArrivalTime.setFont(new Font("Tahoma", Font.PLAIN, 15));
        lblMaxArrivalTime.setBounds(23, 254, 147, 30);
        contentPane.add(lblMaxArrivalTime);

        maxArrTime = new JTextField();
        maxArrTime.setColumns(10);
        maxArrTime.setBounds(180, 265, 86, 19);
        contentPane.add(maxArrTime);
    }

    public void addActionListener(ActionListener actionListener){
        this.startBtn.addActionListener(actionListener);
    }

    public SelectionPolicy getItemFromCombo(){
        return ((SelectionPolicy)this.comboBox.getSelectedItem());
    }

    public int getTimeLimit(){
        return Integer.parseInt(this.timeLimit.getText());
    }

    public int getNbOfServers(){
        return Integer.parseInt(this.nbOfSvs.getText());
    }

    public int getNbOfClients(){
        return Integer.parseInt(this.nbOfClients.getText());
    }

    public int getMaxProcTime(){
        return Integer.parseInt(this.maxProcTime.getText());
    }

    public int getMinProcTime(){
        return Integer.parseInt(this.minProcTime.getText());
    }

    public int getMaxTasks(){
        return Integer.parseInt(this.maxTasksPerSv.getText());
    }

    public int getMinArrTime(){
        return Integer.parseInt(this.minArrTime.getText());
    }

    public int getMaxArrTime(){
        return Integer.parseInt(this.maxArrTime.getText());
    }
}
