import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class SimulationManager extends TimerTask implements Runnable {
    public int timeLimit;
    public int maxProcessingTime;
    public int minProcessingTime;
    public int minArrivalTime;
    public int maxArrivalTime;
    public int numberOfServers;
    public int numberOfClients;
    public int maxTasks;
    public float avgProcessingTime;
    public SelectionPolicy selectionPolicy;

    private StringBuilder details;
    private Scheduler scheduler;

    private SimulationFrame frame;
    private ResultFrame resFrame;

    private List<Task> generatedTasks;
    private List<Thread> generatedThreads;
    private String outputFile;
    private boolean inFile;

    public SimulationManager(String inputFile, String outputFile) throws FileNotFoundException {
        this.inFile = true;
        this.outputFile = outputFile;
        File f1 = new File(inputFile);
        File f2 = new File(outputFile);
        try{
            f2.createNewFile();
        }catch(IOException ex){
            ex.printStackTrace();
        }
        Scanner x = new Scanner(f1);
        this.numberOfClients = x.nextInt();
        this.numberOfServers = x.nextInt();
        this.timeLimit = x.nextInt();
        this.minArrivalTime = x.nextInt();
        this.maxArrivalTime = x.nextInt();
        this.minProcessingTime = x.nextInt();
        this.maxProcessingTime = x.nextInt();
        this.maxTasks = 0;
        x.close();

        scheduler = new Scheduler(numberOfServers,maxTasks);
        generatedTasks = new ArrayList<Task>();
        generatedThreads = new ArrayList<Thread>();
        selectionPolicy = SelectionPolicy.SHORTEST_QUEUE;
        scheduler.changeStrategy(selectionPolicy);
        generateNRandomTasks();
        float avgService = 0;
        for(Task y : generatedTasks){
            avgService += y.getProcessingTime();
        }
        avgProcessingTime = avgService / this.numberOfClients;
        for(int i = 0 ; i < numberOfServers ; i++){
            Thread t = new Thread(scheduler.getServers().get(i));
            generatedThreads.add(t);
            t.start();
        }

    }

    public SimulationManager(SimulationFrame frame, ResultFrame resFrame){
        this.inFile = false;
        this.frame = frame;
        this.resFrame = resFrame;
        timeLimit = frame.getTimeLimit();
        maxProcessingTime = frame.getMaxProcTime();
        minProcessingTime = frame.getMinProcTime();
        minArrivalTime = frame.getMinArrTime();
        maxArrivalTime = frame.getMaxArrTime();
        numberOfServers = frame.getNbOfServers();
        numberOfClients = frame.getNbOfClients();
        scheduler = new Scheduler(numberOfServers,maxTasks);
        generatedTasks = new ArrayList<Task>();
        generatedThreads = new ArrayList<Thread>();
        selectionPolicy = frame.getItemFromCombo();
        scheduler.changeStrategy(selectionPolicy);
        generateNRandomTasks();
        float avgService = 0;
        for(Task x : generatedTasks){
            avgService += x.getProcessingTime();
        }
        avgProcessingTime = avgService / this.numberOfClients;
        for(int i = 0 ; i < numberOfServers ; i++){
            Thread t = new Thread(scheduler.getServers().get(i));
            generatedThreads.add(t);
            t.start();
        }
    }

    private void generateNRandomTasks(){
        for(int i = 0 ; i < this.numberOfClients ; i++){
            Random x = new Random();
            int newArrivalTime = x.nextInt(maxArrivalTime - 1) + minArrivalTime;
            int newProcessingPeriod = x.nextInt(maxProcessingTime - 1) + minProcessingTime;
            Task t = new Task(i+1,newArrivalTime,newProcessingPeriod);
            generatedTasks.add(t);
        }
        generatedTasks.sort(Comparator.comparing(Task::getArrivalTime));
    }

    @Override
    public void run() {
        StringBuilder rez;
        details = new StringBuilder();
        int peakHour = 0;
        int maxPeakHour = 0;
        int currentTime = 0;
        while(currentTime < timeLimit){
            rez = new StringBuilder(50000);
            for(Iterator<Task> x = generatedTasks.iterator() ; x.hasNext();){
                Task y = x.next();
                if(currentTime == y.getArrivalTime()){
                    scheduler.dispatchTask(y);
                    x.remove();
                }
            }
            rez.append("Time ");
            rez.append(currentTime);
            rez.append("\n");
            rez.append("WaitingClients: ");
            for(Task x : generatedTasks){
                rez.append(x);
            }
            rez.append("\n");
            for(Server x : scheduler.getServers()){
                rez.append(x);
                rez.append("\n");
            }

            for(Server x : scheduler.getServers()){
                if(!x.getTasks().isEmpty()){
                    x.getTasks().peek().setProcessingTime(x.getTasks().peek().getProcessingTime() - 1);
                    x.setWaitingPeriod(new AtomicInteger((x.getWaitingPeriod().decrementAndGet())));
                }
            }
            int sum = 0;
            for(Server x : scheduler.getServers()){
                sum += x.getWaitingPeriod().intValue();
            }
            if(sum > maxPeakHour){
                peakHour = currentTime;
                maxPeakHour = sum;
            }
            currentTime++;
            if(!inFile){
                this.resFrame.setResLabel(details);
            }
            details = new StringBuilder(rez.toString());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        float avg = 0;
        for(Server x : scheduler.getServers()){
            avg += x.getAvgWait();
        }
        avg = avg / numberOfClients;
        details.append("Simulation ended!\nAverage waiting time: ");
        details.append(avg);
        details.append("\nAverage processing time: ");
        details.append(this.avgProcessingTime);
        details.append("\nPeak hour: ");
        details.append(peakHour);
        if(inFile){
            try {
                FileWriter a = new FileWriter(outputFile);
                a.write(details.toString());
                a.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else{
            this.resFrame.setResLabel(details);
        }
    }
}
