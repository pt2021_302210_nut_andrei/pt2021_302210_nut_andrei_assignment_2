import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyTime implements Strategy{
    @Override
    public void addTask(List<Server> servers, Task t) {
        int minWait = 0;
        for(Server x : servers){
            if(x.getTasks().isEmpty()){
                Thread th = new Thread(servers.get(servers.indexOf(x)));
                servers.get(servers.indexOf(x)).addTask(t);
                th.start();
                return;
            }
            if(x.getWaitingPeriod().intValue() < servers.get(minWait).getWaitingPeriod().intValue()){
                minWait = servers.indexOf(x);
            }
        }
        servers.get(minWait).addTask(t);
    }
}
