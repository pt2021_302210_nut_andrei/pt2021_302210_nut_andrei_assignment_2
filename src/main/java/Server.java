import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server extends TimerTask implements Runnable{

    private int id;
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private AtomicInteger avgWait;

    public Server(int id){
        this.id = id;
        waitingPeriod = new AtomicInteger(0);
        avgWait = new AtomicInteger(0);
        tasks = new LinkedBlockingQueue<Task>();
        }

    public BlockingQueue<Task> getTasks(){
        return this.tasks;
    }

    public void addTask(Task newTask){
        tasks.add(newTask);
        waitingPeriod.addAndGet(newTask.getProcessingTime());
        avgWait.addAndGet(waitingPeriod.intValue());
    }

    public int getAvgWait(){
        return this.avgWait.intValue();
    }

    public void run(){
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(this.tasks.isEmpty()){
                break;
            }
            if(this.tasks.peek().getProcessingTime() <= 0){
                this.tasks.poll();
            }
        }
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod){
        this.waitingPeriod = waitingPeriod;
    }

    public AtomicInteger getWaitingPeriod(){
        return this.waitingPeriod;
    }

    @Override
    public String toString(){
        if(tasks.isEmpty()){
            return "Queue " + this.id + ": closed";
        }
        else{
            return "Queue " + this.id + ": " + tasks;
        }
    }
}
