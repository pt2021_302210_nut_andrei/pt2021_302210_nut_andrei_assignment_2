public class Task {
    private int arrivalTime;
    private int processingTime;
    private int finishTime;
    private int id;

    public Task(int id,int arrivalTime, int processingTime){
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
    }

    public int getId(){
        return this.id;
    }

    public void setFinishTime(int waitingPeriodOnServer){
        this.finishTime = this.arrivalTime + this.processingTime + waitingPeriodOnServer;
    }

    public void setProcessingTime(int value){
        this.processingTime = value;
    }

    public int getArrivalTime(){
        return this.arrivalTime;
    }

    public int getProcessingTime(){
        return this.processingTime;
    }

    public int getFinishTime(){
        return this.finishTime;
    }

    @Override
    public String toString(){
        return "(" + this.id + ", " + this.arrivalTime + ", " + this.processingTime + ") ";
    }

}
