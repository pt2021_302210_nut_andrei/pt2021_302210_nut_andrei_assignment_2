import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

public class MainClass {

    public static void main(String[] args) throws FileNotFoundException {
        SimulationFrame frame = new SimulationFrame();
        ResultFrame resFrame = new ResultFrame();
        frame.setVisible(true);
        frame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimulationManager y = new SimulationManager(frame,resFrame);
                Thread t = new Thread(y);
                t.start();
                frame.setVisible(false);
                resFrame.setVisible(true);
            }
        });
        //SimulationManager x = new SimulationManager("src\\main\\input1.txt","src\\main\\output1.txt");
        //SimulationManager x = new SimulationManager("src\\main\\input2.txt","src\\main\\output2.txt");
//        SimulationManager x = new SimulationManager("src\\main\\input3.txt","src\\main\\output3.txt");
//        Thread t = new Thread(x);
//        t.start();
    }
}
